#include <iostream>

using namespace std;

class Animal
{
    string Sound = "Meow";
public:
    virtual void Voice()
    {
        cout << Sound << endl;
    }
};
class Cat : public Animal
{
    string Sound = "Meow!";
public:
    void Voice() override
    {
        cout << Sound << endl;
    }
};
class Dog : public Animal
{
    string Sound = "Woof!";
public:
    void Voice() override
    {
        cout << Sound << endl;
    }
};
class Fox : public Animal
{
    string Sound = "WTF?!";
public:
    void Voice() override
    {
        cout << Sound << endl;
    }
};
int main(int argc, char* argv[])
{
    Animal* Animals[3];
    
    Animals[0] = new Cat;
    Animals[1] = new Dog;
    Animals[2] = new Fox;

    for (const auto Animal : Animals)
    {
        Animal->Voice();
    }
        
    return 0;
}
